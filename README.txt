Twitter Embedded Timeline is a Drupal 6 module which allows administrators
to create a block which display embedded timelines.

Set the widget parameters in admin/settings/twitter-embedded-timeline.

Get the Widget ID in the HTML widget code when you generete the widget.
