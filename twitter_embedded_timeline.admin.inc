<?php
/**
 * @file
 * Administration page callbacks
 */

/**
 * Form builder. Configure Social Network settings.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function twitter_embedded_timeline_admin_settings() {
  $form['twitter_embedded_timeline'] = array(
    '#type' => 'fieldset',
    '#title' => t('Twitter Widget settings'),
    '#collapsible' => FALSE,
  );
  $form['twitter_embedded_timeline']['twitter_embedded_timeline_theme'] = array(
    '#type' => 'select',
    '#title' => 'Widget Theme',
    '#default_value' => variable_get('twitter_embedded_timeline_theme'),
    '#options' => array(
      'light' => t('Light'),
      'dark'  => t('Dark'),
    ),
  );
  $form['twitter_embedded_timeline']['twitter_embedded_timeline_widget_id']
    = array(
      '#type' => 'textfield',
      '#title' => t('Twitter Widget ID'),
      '#default_value' => variable_get('twitter_embedded_timeline_widget_id'),
      '#required' => TRUE,
    );
  $form['twitter_embedded_timeline']['twitter_embedded_timeline_screen_name']
    = array(
      '#type' => 'textfield',
      '#title' => t('Twitter Username'),
      '#default_value' => variable_get('twitter_embedded_timeline_screen_name', 'twitterapi'),
      '#description' => t('Enter the Twitter Username. I.e.: twitterapi'),
      '#required' => TRUE,
    );
  $form['twitter_embedded_timeline']['twitter_embedded_timeline_width']
    = array(
      '#type' => 'textfield',
      '#title' => t('Twitter Widget width'),
      '#default_value' => variable_get('twitter_embedded_timeline_width', '220'),
      '#description' => t("Set Twitter's Widget width"),
    );
  $form['twitter_embedded_timeline']['twitter_embedded_timeline_height']
    = array(
      '#type' => 'textfield',
      '#title' => t('Twitter Widget height'),
      '#default_value' => variable_get('twitter_embedded_timeline_height', '300'),
      '#description' => t("Set Twitter's Widget height"),
    );
  $form['twitter_embedded_timeline']['twitter_embedded_timeline_link_color']
    = array(
      '#type' => 'textfield',
      '#title' => t('Link color'),
      '#default_value' => variable_get('twitter_embedded_timeline_link_color', '#00000'),
      '#description' => t('Set the links color inside the widget. Note that
        some icons in the widget will also appear this color.
        Hex format color'),
    );
  $form['twitter_embedded_timeline']['twitter_embedded_timeline_border_color']
    = array(
      '#type' => 'textfield',
      '#title' => t('Border color'),
      '#default_value' => variable_get('twitter_embedded_timeline_border_color', '#00000'),
      '#description' => t('Set the widget border color. Hex format color.'),
    );
  $form['twitter_embedded_timeline']['twitter_embedded_timeline_limit']
    = array(
      '#type' => 'textfield',
      '#title' => t('Tweet limit'),
      '#default_value' => variable_get('twitter_embedded_timeline_limit', 0),
      '#description' => t('To fix the size of a timeline to a preset number
        of Tweets. Use any value between 1 and 20 Tweets. To don\'t limit
        use 0 (zero).'),
    );
  $form['twitter_embedded_timeline']['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Options'),
    '#collapsible' => FALSE,
  );
  $form['twitter_embedded_timeline']['options']['twitter_embedded_timeline_show_replies'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show replies'),
    '#default_value' => 1,
  );
  $form['twitter_embedded_timeline']['options']['twitter_embedded_timeline_chrome']
    = array(
      '#type' => 'checkboxes',
      '#title' => t('Chrome'),
      '#default_value' => variable_get('twitter_embedded_timeline_chrome'),
      '#options' => array(
        'noheader'    => t('No header'),
        'nofooter'    => t('No Footer'),
        'noborders'   => t('No borders'),
        'noscrollbar' => t('No scrollbar'),
        'transparent' => t('Transparent'),
      ),
    );

  return system_settings_form($form);
}

/**
 * Validates the Twitter Widget Setting admin configuration form.
 */
function twitter_embedded_timeline_admin_settings_validate($form, $form_state) {

  $twitter_width = $form_state['values']['twitter_embedded_timeline_width'];
  if (!is_numeric($twitter_width) || intval($twitter_width) <= 0) {
    form_set_error('twitter_embedded_timeline_width', t('Twitter Widget width should be a number bigger than 0'));
  }

  $twitter_height = $form_state['values']['twitter_embedded_timeline_height'];
  if (!is_numeric($twitter_height) || intval($twitter_height) <= 0) {
    form_set_error('twitter_embedded_timeline_height', t('Twitter Widget height should be a number bigger than 0'));
  }

  $link_color = $form_state['values']['twitter_embedded_timeline_link_color'];
  if (!_twitter_embedded_timeline_validate_rgb_color($link_color)) {
    form_set_error('twitter_embedded_timeline_link_color', t('Link color should be
      a valid RGB value'));
  }

  $border_color = $form_state['values']['twitter_embedded_timeline_border_color'];
  if (!_twitter_embedded_timeline_validate_rgb_color($border_color)) {
    form_set_error('twitter_embedded_timeline_border_color', t('Border color should be
      a valid RGB value'));
  }

  $tweets_limit = $form_state['values']['twitter_embedded_timeline_limit'];
  if ($tweets_limit < 0 || $tweets_limit > 20) {
    form_set_error('twitter_embedded_timeline_limit', t('Tweets limit should be any value between 0 and 20 Tweets'));
  }
}

/**
 * Validates an RGB color.
 *
 *  Hex colors must start with '#' and have 6 characters.
 *  Hex colors can contain the following letters: [A-F].
 *  Hex colors can contain the following digits: [0-9].
 */
function _twitter_embedded_timeline_validate_rgb_color($color) {
  if ($color == '#00000') {
    $is_valid = TRUE;
  }
  else {
    $is_valid = preg_match('/^#[a-f0-9]{6}$/i', $color);
  }
  return $is_valid;
}
