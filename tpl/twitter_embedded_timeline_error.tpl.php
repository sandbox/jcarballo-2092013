<?php
/**
 * @file
 * Print a error to the old IE version ..
 */
?>
<div class="twitter-embedded-timeline ie7-error">
  <div class="inner">
    <?php echo $twitter_error_message ?>
  </div>
</div>
