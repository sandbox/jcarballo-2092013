<?php
/**
 * @file
 * Print the Twitter Embedded Timeline Block.
 */
?>
<div class="twitter-embedded-timeline">
  <?php
    print l(
      t('Tweets by @') . $twitter_setting['screen_name'],
      'https://twitter.com/' . $twitter_setting['screen_name'],
      array(
        'attributes' => array(
          'class'             => 'twitter-timeline',
          'data-widget-id'    => $twitter_setting['widget_id'],
          'data-screen-name'  => $twitter_setting['screen_name'],
          'data-theme'        => $twitter_setting['theme'],
          'data-link-color'   => $twitter_setting['link_color'],
          'width'             => $twitter_setting['width'],
          'height'            => $twitter_setting['height'],
          'data-chrome'       => implode(' ', array_filter($twitter_setting['chrome'])),
          'data-border-color' => $twitter_setting['border_color'],
          'data-tweet-limit'  => $twitter_setting['tweets_limit'],
          'data-show-replies' => $twitter_setting['show_replies'],
        ),
      )
    );
  ?>
</div>
